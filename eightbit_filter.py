"""

This module contains filter functions to modify images

"""

from PIL import Image, ImageDraw, ImageFont, ImageEnhance

def eightBitify(img, text):
    """ Create an 8 bit version of the image. """

    # Setting the pixelation range
    baseWidth=180
    peakWidth=500

    # Setting the text settings
    stringBreakPosition = 43
    textIndent = 30
    textStart = 305
    textColor = (255,255,255)
    textFont = ImageFont.truetype('pixel.ttf', 10)

    # Creating the 8 bit image
    contrast = ImageEnhance.Contrast(img)
    img = contrast.enhance(1.5)
    brightness = ImageEnhance.Brightness(img)
    img = brightness.enhance(1.5)
    img = resizeImage(img, baseWidth)
    img = resizeImage(img, peakWidth)

    # Finding the center of the image to crop it
    center = img.size[1] / 2
    img = img.crop((0, center - 85, 500, center + 100))

    # Pasting the image in a new widescreen canvas
    newImg = Image.new("RGB", (500,375), "black")
    newImg.paste(img,(0, 95, 500, 280))

    # Adding text to the image
    draw = ImageDraw.Draw(newImg)
    for line in text.splitlines():
        lastPosition = 0
        for i in xrange(0, len(line), 43):
            breakPosition = i + 43
            if (breakPosition > len(text)):
                breakPosition = len(text)
            else:
                while(text[breakPosition - 1] != " "):
                    breakPosition -= 1
            draw.text((textIndent, textStart), line[lastPosition:breakPosition], textColor, font=textFont)
            lastPosition = breakPosition
            textStart += 10

    return newImg

def resizeImage(img, width):
    wpercent = (width/float(img.size[0]))
    height = int((float(img.size[1])*float(wpercent)))
    img = img.resize((width, height))
    return img

if __name__ == "__main__":

    # load a test image
    text = "I am trying to create a sentence or two which have 140 characters. Because I want to test it with my new twitter bot. I hope the bot is cool."
    img = Image.open("bike.jpg")

    # blur the image
    img2 = eightBitify(img, text)
    img2.show()
