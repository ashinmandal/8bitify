# 8Bitify
Python code that creates 8 bit cut scenes from text and image posted to the bot.

Added [craftoid](https://github.com/craftoid)'s [twitterbot-examples](https://github.com/craftoid/twitterbot-examples) as a sub-module.

### Normal clone:
`git clone git@github.com:ashinmandal/8bitify.git`

### Clone with sub-module:
`git clone --recursive git@github.com:ashinmandal/8bitify.git`

### Information about sub-modules:
https://blog.github.com/2016-02-01-working-with-submodules/

NOTE: "keys.py" is not included in this repository and must be manually added and populated with the **consumer key** and **access token** for [**@8bitify**](https://twitter.com/8bitify) twitter account as variables for it's connection with the Twitter Apps API. The keys could also be declared as environment variables and imported in python.

### Variable Structure:
```
consumer_key = "XXXXXXXXXXXXXXX"
consumer_secret = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
access_token = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
access_token_secret = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
```

If they keys / access tokens are lost, go to https://apps.twitter.com/ and login with the respective bot credential to retrieve them.
