"""

This module contains filter functions to modify images

"""

from PIL import Image, ImageDraw, ImageFont, ImageEnhance
# import numpy as np
# import cv2
# from matplotlib import pyplot as plt

def eightBitify(img, text):
    """ Create an 8 bit version of the image. """
    width = img.size[0]
    height = img.size[1]

    img.save('temp.png')

    with open("temp.png", "rb") as imageFile:
        f = imageFile.read()
        b = bytearray(f)
    print img.size[0]

    intensityInfo = [[foo for i in range(len(b))]for bar in range(2)]

    print b[3*img.size[0]+4]
    for x in range(0,width):
        for y in range(0,height):
            coordinate = (x, y)
            intensityInfo = [b[x*width+y], coordinate]

    print(intensityInfo)
    # print((img[3*1000+4] & 0xFF))
    # img.save('temp.png')
    # img_cv = cv2.imread('temp.png',0)
    # hist = cv2.calcHist([img_cv],[0],None,[256],[0,256])
    # print(hist)

    # Setting the pixelation range
    baseWidth=180
    peakWidth=500

    # Setting the text settings
    stringBreakPosition = 43
    textIndent = 30
    textStart = 305
    textColor = (255,255,255)
    textFont = ImageFont.truetype('pixel.ttf', 10)

    # Creating the 8 bit image
    contrast = ImageEnhance.Contrast(img)
    img = contrast.enhance(1.5)
    brightness = ImageEnhance.Brightness(img)
    img = brightness.enhance(1.5)
    img = resizeImage(img, baseWidth)
    img = resizeImage(img, peakWidth)

    # img_cv = cv2.imread('temp.png',0)
    #
    # # Initiate FAST object with default values
    # fast = cv2.FastFeatureDetector(180)
    #
    # # find and draw the keypoints
    # kp = fast.detect(img_cv,None)
    # for keypoints in kp:
    #     yCropCalc = yCropCalc + int(keypoints.pt[1])
    #
    # img2 = cv2.drawKeypoints(img_cv, kp, color=(255,0,0))
    # cv2.imwrite('fast_true.png',img2)

    # Cropping the image
    img = img.crop((0, 25, 500, 210))

    # Pasting the image in a new widescreen canvas
    newImg = Image.new("RGB", (500,375), "black")
    newImg.paste(img,(0, 95, 500, 280))

    # Adding text to the image
    draw = ImageDraw.Draw(newImg)
    for line in text.splitlines():
        lastPosition = 0
        for i in xrange(0, len(line), 43):
            breakPosition = i + 43
            if (breakPosition > len(text)):
                breakPosition = len(text)
            else:
                while(text[breakPosition - 1] != " "):
                    breakPosition -= 1
            draw.text((textIndent, textStart), line[lastPosition:breakPosition], textColor, font=textFont)
            lastPosition = breakPosition
            textStart += 10

    return newImg

def resizeImage(img, width):
    wpercent = (width/float(img.size[0]))
    height = int((float(img.size[1])*float(wpercent)))
    img = img.resize((width, height))
    return img

if __name__ == "__main__":

    # load a test image
    text = "I am trying to create a sentence or two which have 140 characters. Because I want to test it with my new twitter bot. I hope the bot is cool."
    img = Image.open("bike.jpg")

    # blur the image
    img2 = eightBitify(img, text)
    img2.save("bike_8bit.jpg")
