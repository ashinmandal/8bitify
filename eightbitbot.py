#
# Copyright 2015, Catastrophe Corporation
# eightbitbot.py
# Author: Ashin Mandal (ashin.mandal@gmail.com)
# Version:  2.0
# Description: Python bot that creates 8 bit cut scenes from text and image
#

# Imports
import datetime
from time import sleep
import tweepy
from keys import *
from eightbit_filter import eightBitify
from image import get_image_file
from PIL import Image
import StringIO
import aiml
import random

# Initialisations
flag = 0
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)
botBrain = aiml.Kernel()
botBrain.learn("replies.aiml")
defaultReplies = ["I did not really get that. Try again?", "I got confused. Let's give it another shot.", "Erm.. not really sure. Try once again?", "Wait. I did not get that. Let's try from the beginning.", "Processing.. Processing.. Not processed! Error 404! Try one more time please!"]
defaultSuccessReplies = ["{}, your image has been 8bitified! Want to send another one?", " {}, I 8bitified your image. Do you like it?", "{}, Here you go. One 8bitified image for you. Want to continue?", "{}, I just 8bitified your image. Let's make more 8bit poetry!"]

def bot():
    while 1:
        try:
            mentions = api.mentions_timeline(count=10)
            for tweet in mentions:

                mt = tweet.created_at
                st = datetime.datetime.utcnow()
                # Tweet back to the user if there is a mention within the last 60 seconds
                if (st-mt) < datetime.timedelta(seconds=60):
                    print("Mention: " + tweet.text)
                    eightBitText = ((str(tweet.text).split(" ", 1))[1]).rsplit(" ", 1)[0]
                    prefix = "@" + tweet.user.screen_name
                    # get image from the tweet
                    try:

                        image_file = get_image_file(tweet)

                        if image_file is None:
                            botSays = botBrain.respond(eightBitText)
                            if len(botSays) == 0:
                                text = prefix + " " + random.choice(defaultReplies)
                            else:
                                text = prefix + " " + botSays
                            # text = "{}, you did not attach an image with your tweet. I don't have anything to 8bitify for you!".format(prefix)
                        else:
                            # create a tweet and make sure to cut it off at 140 chars
                            text = random.choice(defaultSuccessReplies).format(prefix)

                    except Exception as e:
                        print(e)


                    # do the tweeting based on wether we have an image
                    tweetsize = 140 - len(prefix) - 1
                    text = text[:140]


                    if image_file is None:

                        # post a default reply
                        print("No Image received")
                        api.update_status(status=text, in_reply_to_status_id=tweet.id)
                        sleep(60)
                        continue

                    else:

                        # image processing
                        filename, file = image_file
                        img = Image.open(file)

                        # 8bitifying the image
                        print("8bitifying the image")
                        img = eightBitify(img, eightBitText)

                        # save the image back to a new file, using the original file format
                        print("Saving the image")
                        format = filename.split(".", 1)[1].upper()
                        print("Image format: {}".format(format))
                        file = StringIO.StringIO()
                        img.save(file, format="PNG")

                    try:
                        # post the tweet
                        print("Posting the image")
                        api.update_with_media(status=text, in_reply_to_status_id=tweet.id, filename=filename, file=file)

                    except Exception as e:
                        # did anything go wrong when we tried to create and post the tweet?
                        print(e)

            sleep(60)
        except (KeyboardInterrupt, IOError):
            break

if __name__ == '__main__':
    # running the bot
    print("Running the bot")
    bot()
